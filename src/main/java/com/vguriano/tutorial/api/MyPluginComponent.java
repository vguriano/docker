package com.vguriano.tutorial.api;

public interface MyPluginComponent
{
    String getName();
}