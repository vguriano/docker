package ut.com.vguriano.tutorial;

import org.junit.Test;
import com.vguriano.tutorial.api.MyPluginComponent;
import com.vguriano.tutorial.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}